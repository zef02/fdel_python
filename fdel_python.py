# -*- coding: utf-8 -*-

import os
import time
import shutil
import datetime
import glob

#対象フォルダ
#DeleteTargetFolder = "C:\\or_server\\PDF_Make\\data\\OK\\"
DeleteTargetFolder = "C:\\SERVER\\PRSOS001\\or_server\\event\\schema\\000000000000000000\\"


# 削除更新日
DDays = 30

#対象ファイル
TargetFile = "*.*"

#TODO サブフォルダ内も見に行くか
#subFolderTargetFlg = True
#TODO 空になったフォルダは削除するか
# FolderEmptyDeleteFlg = True

#削除対象日を取得
dtNow = datetime.datetime.now()
dtdeleteday = dtNow - datetime.timedelta(days=DDays)


list = glob.glob(DeleteTargetFolder + TargetFile)
for file in list:

    #ファイルの最終更新日を取得
    fileproparty = os.stat(file)
    last_modified = fileproparty.st_mtime
    dt = datetime.datetime.fromtimestamp(last_modified)

    if dtdeleteday > dt:
        #ファイルを削除
        os.remove(file)
